-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Sep 2023 pada 17.19
-- Versi server: 10.4.17-MariaDB
-- Versi PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_omni`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('Admin', '1', 1694087734);

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rule_name` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, 1694087702, 1694087702),
('/default/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/default/captcha', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/default/error', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/default/index', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/default/login', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/default/logout', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/gii/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/gii/default/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/gii/default/action', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/gii/default/diff', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/gii/default/index', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/gii/default/preview', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/gii/default/view', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/gridview/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/gridview/export/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/gridview/export/download', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/gridview/grid-edited-row/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/gridview/grid-edited-row/back', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/gridviewKrajee/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/gridviewKrajee/export/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/gridviewKrajee/export/download', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/gridviewKrajee/grid-edited-row/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/gridviewKrajee/grid-edited-row/back', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/menu/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/menu/create', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/menu/delete', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/menu/index', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/menu/update', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/menu/view', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/pesanan/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/pesanan/create', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/pesanan/delete', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/pesanan/index', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/pesanan/update', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/pesanan/view', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/produk/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/produk/index', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/produk/view', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/assignment/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/assignment/assign', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/assignment/index', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/assignment/revoke', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/assignment/view', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/default/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/default/index', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/menu/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/menu/create', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/menu/delete', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/menu/index', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/menu/update', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/menu/view', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/permission/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/permission/assign', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/permission/create', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/permission/delete', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/permission/index', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/permission/remove', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/permission/update', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/permission/view', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/role/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/role/assign', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/role/create', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/role/delete', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/role/index', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/role/remove', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/role/update', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/role/view', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/route/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/route/assign', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/route/create', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/route/index', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/route/refresh', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/route/remove', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/rule/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/rule/create', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/rule/delete', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/rule/index', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/rule/update', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/rule/view', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/user/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/user/activate', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/user/change-password', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/user/delete', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/user/index', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/user/login', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/user/logout', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/user/request-password-reset', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/user/reset-password', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/user/signup', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/rbac/user/view', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/site/*', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/site/captcha', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('/site/error', 2, NULL, NULL, NULL, 1694099110, 1694099110),
('Admin', 1, NULL, NULL, NULL, 1694087694, 1694087694);

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('Admin', '/*');

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`id`, `name`, `parent`, `route`, `order`, `data`, `params`, `deskripsi`) VALUES
(1, 'Akses', NULL, NULL, 4, '<i class=\"fal fa-shield-alt\"></i>', NULL, NULL),
(2, 'RBAC', 1, '/rbac/assignment/index', 1, NULL, NULL, NULL),
(3, 'Menu', 1, '/menu/index', 2, NULL, NULL, NULL),
(4, 'Home', NULL, '/default/index', 1, '<i class=\"fal fa-home\"></i>', NULL, ''),
(5, 'Produk', NULL, '/produk/index', 2, '<i class=\"fal fa-database\"></i>', NULL, ''),
(6, 'Pesanan', NULL, '/pesanan/index', 3, '<i class=\"fal fa-truck-pickup\"></i>', NULL, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1694087540),
('m230820_000808_create_table_auth_rule', 1694087542),
('m230820_000812_create_table_menu', 1694087542),
('m230820_000814_create_table_user', 1694087542),
('m230820_000816_create_table_auth_item', 1694087542),
('m230820_000817_create_table_auth_item_child', 1694087542),
('m230820_000819_create_table_auth_assignment', 1694087542),
('m230820_000820_create_foreign_keys', 1694087542);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_pesanan`
--

CREATE TABLE `t_pesanan` (
  `no_pesanan` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `nm_supplier` varchar(50) NOT NULL,
  `nm_produk` varchar(50) NOT NULL,
  `total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `t_pesanan`
--

INSERT INTO `t_pesanan` (`no_pesanan`, `tanggal`, `nm_supplier`, `nm_produk`, `total`) VALUES
('1234', '2023-09-07 10:50:00', 'Apple', 'IPHONE 5', 9500000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'U1HEIDuahClNyda04S-0Ap3NoU2AoC3f', '$2y$13$VWEtNfObOe7KkMq9k5.E9ehHQLjy9hYLO1b9zsxIFvBFUdtVf7Pbi', NULL, '-', 10, '2022-10-08 22:25:20', '2022-10-08 22:25:23');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `idx-auth_assignment-user_id` (`user_id`);

--
-- Indeks untuk tabel `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `idx-auth_item-type` (`type`),
  ADD KEY `auth_item_rule_name_fkey` (`rule_name`);

--
-- Indeks untuk tabel `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `auth_item_child_child_fkey` (`child`);

--
-- Indeks untuk tabel `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indeks untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_parent_fkey` (`parent`);

--
-- Indeks untuk tabel `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indeks untuk tabel `t_pesanan`
--
ALTER TABLE `t_pesanan`
  ADD PRIMARY KEY (`no_pesanan`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_item_name_fkey` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_rule_name_fkey` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_child_fkey` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_parent_fkey` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_parent_fkey` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
