<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$modelClass = StringHelper::basename($generator->modelClass);

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\widgets\Pjax;
use yii\web\View;
use app\widgets\JSRegister;
use <?= $generator->indexWidgetType === 'grid' ? "kartik\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>

<?= "<?php " ?>
yii\bootstrap4\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    'options' => [
        'data-backdrop' => 'static',
        'tabindex'      => false,
    ],
]);
echo "<div id='modalContent'></div>";
yii\bootstrap4\Modal::end();
?>

<div class="col-lg-12 text-right">
    <?= "<?= " ?> Html::button('<i class="fal fa-plus-circle"></i> Tambah', ['value' => Url::to(['create']), 'title' => 'Form '.<?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, 'class' => 'showModalButton btn btn-primary']); ?>
</div>
<hr/>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index col-lg-12 p-0 table-responsive">

<?='<?php'?> Pjax::begin(['id' => 'gridData', 'timeout' => false, 'enablePushState' => true, 'enableReplaceState' => false]); ?>
<?php if(!empty($generator->searchModelClass)): ?>
<?= "    <?php " . ($generator->indexWidgetType === 'grid' ? "// " : "") ?>echo $this->render('_search', ['model' => $searchModel]); ?>
<?php endif; ?>

<?php if ($generator->indexWidgetType === 'grid'): ?>
    <?= "<?= " ?>GridView::widget([
        'dataProvider' => $dataProvider,
        <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n        'columns' => [\n" : "'columns' => [\n"; ?>
            ['class' => 'yii\grid\SerialColumn'],

<?php
$count = 0;
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        if (++$count < 6) {
            echo "            '" . $name . "',\n";
        } else {
            echo "            //'" . $name . "',\n";
        }
    }
} else {
    foreach ($tableSchema->columns as $column) {
        $format = $generator->generateColumnFormat($column);
        if (++$count < 6) {
            echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        } else {
            echo "            //'" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        }
    }
}
?>
            [
                'header'=>'#',
                'format'=>'raw',
                'value' => function ($model) {
                    return Html::button('<i class="fal fa-file-search"></i>', ['value' => Url::to(['view', <?= $generator->generateUrlParams() ?>]), 'title' => 'Detail '.<?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, 'class' => 'showModalButton btn btn-dark mb-2 mr-2 btn-rounded']).
                    Html::button('<i class="fal fa-edit"></i>', ['value' => Url::to(['update', <?= $generator->generateUrlParams() ?>]), 'title' => 'Form Edit '.<?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, 'class' => 'showModalButton btn btn-dark mb-2 mr-2 btn-rounded']).
                    Html::button('<i class="fal fa-trash"></i>', ['<?=$generator->modelClass::primaryKey()[0]?>' => $model-><?=$generator->modelClass::primaryKey()[0]?>, 'title' => 'Hapus Data?', 'class' => 'btn btn-dark mb-2 mr-2 btn-rounded delete']);
                }
            ],
        ],
    ]); ?>
<?php else: ?>
    <?= "<?= " ?>ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model-><?= $generator->getNameAttribute() ?>), ['view', <?= $generator->generateUrlParams() ?>]);
        },
    ]) ?>
<?php endif; ?>

<?="    <?php Pjax::end(); ?>\n"?>

</div>

<?='<?php'?>
    $link_delete               = Url::to(['delete']);
<?="\n"?>
    JSRegister::begin(['position' => View::POS_END]);
?>
    <script>
        var is_fetch = false;
        $(document).on("click", ".delete", function () {
            var id               = $(this).attr("<?=$generator->modelClass::primaryKey()[0]?>");
            var link_delete            = "<?='<?= $link_delete; ?>'?>?<?=$generator->modelClass::primaryKey()[0]?>="+id;
            Swal.fire({
            title: 'Konfirmasi',
            text: 'Apakah Akan Menghapus Data?',
            showCancelButton: true,
            confirmButtonText: 'Lanjutkan',
            cancelButtonText: 'Batal',
            icon: 'question',
            }).then((result) => {
                if (result.isConfirmed) {
                    if(is_fetch == false) {
                        is_fetch = true;
                        $.ajax({
                            type: "POST",
                            url: link_delete,
                            dataType: "html",
                            success: function(response){
                              const backRes = JSON.parse(response);
                              if(backRes.status == 'success') {
                                  Swal.fire('Ok', backRes.message, 'success');
                                  $.pjax.reload({container:"#gridData"});
                              } else {
                                  Swal.fire('Terjadi Kesalahan.', backRes.message, 'warning');
                              }
                              is_fetch = false
                            }
                        });
                    }
                }
            })
        });
    </script>
<?='<?php'?> JSRegister::end(); ?>
