<?php

namespace app\controllers;

use Yii;
use app\models\Pesanan;
use app\models\PesananSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PesananController implements the CRUD actions for Pesanan model.
 */
class PesananController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Pesanan models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PesananSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pesanan model.
     * @param string $no_pesanan No Pesanan
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($no_pesanan)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($no_pesanan),
        ]);
    }

    /**
     * Creates a new Pesanan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Pesanan();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
              Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
              if($model->save()){
                return [
                  'status' => 'success',
                  'message' => 'Berhasil Menambah Data'
                ];
              } else {
                return [
                  'status' => 'failed',
                  'message' => 'Gagal Menambah Data : '.json_encode($model->errors)
                ];
              }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Pesanan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $no_pesanan No Pesanan
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($no_pesanan)
    {
        $model = $this->findModel($no_pesanan);

        if ($this->request->isPost && $model->load($this->request->post())) {
            if ($model->load($this->request->post())) {
              Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
              if($model->save()){
                return [
                  'status' => 'success',
                  'message' => 'Berhasil Mengubah Data'
                ];
              } else {
                return [
                  'status' => 'failed',
                  'message' => 'Gagal Mengubah Data'
                ];
              }
            }
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pesanan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $no_pesanan No Pesanan
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($no_pesanan)
    {
        $model = $this->findModel($no_pesanan);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($model->delete()){
          return [
            'status' => 'success',
            'message' => 'Berhasil Menghapus Data'
          ];
        } else {
          return [
            'status' => 'failed',
            'message' => 'Gagal Menghapus Data'
          ];
        }
    }

    /**
     * Finds the Pesanan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $no_pesanan No Pesanan
     * @return Pesanan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($no_pesanan)
    {
        if (($model = Pesanan::findOne(['no_pesanan' => $no_pesanan])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
