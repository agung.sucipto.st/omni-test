<?php

namespace app\controllers;

use yii\httpclient\Client;
use yii\data\ArrayDataProvider;

class ProdukController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionView($id)
    {
        return $this->renderAjax('view',[
          'id' => $id
        ]);
    }

}
