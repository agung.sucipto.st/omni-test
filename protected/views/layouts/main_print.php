<?php

use yii\helpers\Url;
/** @var $content */

use \yii\helpers\Html;

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title><?= Html::encode($this->title) ?> - <?= Yii::$app->name ?></title>
    <link rel="shortcut icon" href="<?= Url::base(true); ?>/themes/cork/images/favicon.png" type="image/x-icon" />
    <style>
        body {
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        }

        .print {
            margin: auto;
        }

        thead {
            background: #d1d1d1;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        th {
            font-weight: bold;
        }

        td,
        th {
            padding: 5px 10px;
            text-align: left;
            font-size: 11px;
        }
    </style>
</head>

<body>
    <?= $content ?>
    <!--<footer>-->
    <!--    Invoice was created on a computer and is valid without the signature and seal.-->
    <!--</footer>-->
</body>
<script type="text/javascript">
    function printPage() {
        window.print();
    }
    printPage();
</script>

</html>
