<?php
/** @var $content */

use \yii\helpers\Html;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?= Html::encode($this->title) ?> - <?= Yii::$app->name ?></title>
    <style>
        body {
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        }

        .print {
            margin: auto;
        }

        thead {
            background: #d1d1d1;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        th {
            font-weight: bold;
        }

        td, th {
            padding: 5px 10px;
            border: 1px solid #ccc;
            text-align: left;
            font-size: 11px;
        }

        header {
            padding-bottom: 20px;
        }

        .text_right {
            text-align: right;
        }

        table.no_border {
            width: auto;
        }

        table.no_border th {
            border: 0;
        }

        table.no_border td {
            border: 0;
        }

        @media print {
            .print_function {
                display: none !important;
            }
        }

    </style>
    <link rel="stylesheet" href="http://cdn.webix.com/edge/webix.css" type="text/css">
    <script src="http://cdn.webix.com/edge/webix.js" type="text/javascript"></script>
</head>
<body>
<div>
    <button class="print_function" onclick="printPage()">Print</button>
</div>
<?= $content ?>
<!--<footer>-->
<!--    Invoice was created on a computer and is valid without the signature and seal.-->
<!--</footer>-->
</body>
<script type="text/javascript">
    function printPage() {
        window.print();
    }
</script>
</html>
