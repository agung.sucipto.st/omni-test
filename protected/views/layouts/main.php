<?php

/* @var $this \yii\web\View */
/* @var $content string */
use app\assets\CorkAsset;
use app\assets\MDFontAsset;
use app\widgets\Alert;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use edwinhaq\simpleloading\SimpleLoading;

CorkAsset::register($this);
MDFontAsset::register($this);
SimpleLoading::widget();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> - <?=Yii::$app->name?></title>
    <link rel="shortcut icon" href="<?= Url::base(true); ?>/themes/cork/images/favicon.png" type="image/x-icon" />
    <?php $this->head() ?>
</head>

<body class="sidebar-noneoverflow starterkit">
    <?php $this->beginBody() ?>

    <!--  BEGIN NAVBAR  -->
    <?= $this->render("components/navbar") ?>
    <!--  END NAVBAR  -->

    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

        <!--  BEGIN SIDEBAR  -->
        <?php
        $sidebar = "components/sidebar";

        ?>
        <?= $this->render($sidebar) ?>
        <!--  END SIDEBAR  -->

        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">

                <?= Alert::widget() ?>
                <div class="layout-top-spacing">
                <div class="statbox widget box box-shadow">
                    <?= $content ?>
                </div>
                </div>

            </div>
            <div class="footer-wrapper">
                <div class="footer-section f-section-1">
                    <p class="">Copyright © 2022 <a target="_blank" href="#">Weighting</a>, All rights reserved.</p>
                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->

    </div>
    <!-- END MAIN CONTAINER -->

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
