<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\LoginAsset;
use app\widgets\Alert;
use yii\bootstrap4\Html;
use yii\helpers\Url;

LoginAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> - <?= Yii::$app->name ?></title>
    <link rel="shortcut icon" href="<?= Url::base(true); ?>/themes/cork/images/favicon.png" type="image/x-icon"/>
    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>
<div id="overlay"></div>
<!--  BEGIN MAIN CONTAINER  -->
<div class="container-fluid">
<div class="row">
  <div class="col-sm-4 col-md-4"></div>
  <div class="col-sm-4 col-md-4">
    <div class="row">
      <div class="col-sm-12">
        <?=$content?>
      </div>
    </div>
  </div>
  <div class="col-sm-4 col-md-4"></div>
</div>
</div>
<!-- END MAIN CONTAINER -->

<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
