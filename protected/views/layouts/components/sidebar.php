<?php

use yii\helpers\Url;
use app\modules\rbac\components\MenuHelper;
use yii\helpers\Json;

$menus = MenuHelper::getAssignedMenu(
  Yii::$app->user->id,
  null,
  function ($menu) {
    $urlParams = [$menu['route']];
    $json = $menu['params'];
    if (!empty($json)) {
      $param = Json::decode($json, $asArray = true);
      foreach ($param as $key => $value) {
        $urlParams[$key] = $value;
      }
    }
    return [
      'id' => $menu['id'],
      'label' => $menu['name'],
      'url' => $urlParams,
      'icon' => $menu['data'],
      'order' => $menu['order'],
      'children' => $menu['children'],
    ];
  }
);

yii\bootstrap4\Modal::begin([
  'headerOptions' => ['id' => 'modalHeader'],
  'id' => 'modal',
  'size' => 'modal-xl',
  'options' => [
    'data-backdrop' => 'static',
    'tabindex'      => false,
  ],
]);
echo "<div id='modalContent'></div>";
yii\bootstrap4\Modal::end();
// echo '<pre>';
// print_R($menus);
// exit;

?>

<style>
  .pointer {
    cursor: pointer;
  }
</style>
<div class="sidebar-wrapper sidebar-theme">
  <nav id="compactSidebar">

    <div class="theme-logo">
      <a href="<?= Url::to(['/default/index']) ?>">
        <img src="<?= Url::base(true); ?>/themes/cork/images/app_icon.png" class="navbar-logo" alt="logo">
      </a>
    </div>

    <ul class="menu-categories">

      <?php
      $submenu = '';
      $menu = '';
      foreach ($menus as $row) {
        if (empty($row['children'])) {
          $menu .= '
                    <li class="menu menu-single">
                        <a href="' . Url::to($row['url']) . '" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    ' . $row['icon'] . '
                                </div>
                            </div>
                        </a>
                        <div class="tooltip"><span>' . $row['label'] . '</span></div>
                    </li>
                  ';
        } else {
          $menu .= '
                    <li class="menu">
                      <a href="#submenu-' . $row['id'] . '" data-active="false" class="menu-toggle">
                          <div class="base-menu">
                              <div class="base-icons">
                                  ' . $row['icon'] . '
                              </div>
                          </div>
                      </a>
                      <div class="tooltip"><span>' . $row['label'] . '</span></div>
                  </li>
                  ';

          $submenu .= '
                  <div class="submenu" id="submenu-' . $row['id'] . '">
                      <div class="menu-title">
                          <h3>' . $row['label'] . '</h3>
                      </div>
                      <ul class="submenu-list" data-parent-element="#submenu-' . $row['id'] . '">';

          foreach ($row['children'] as $sub) {
            if (empty($sub['children'])) {
              $submenu .= '
                              <li>
                                  <a href="' . Url::to($sub['url']) . '"> ' . $sub['label'] . ' </a>
                              </li>';
            } else {
              $submenu .= '
                                <li class="sub-submenu">
                                  <a role="menu" class="collapsed" data-toggle="collapse" data-target="#submenu-' . $row['id'] . '-' . $sub['id'] . '"
                                    aria-expanded="false">
                                      <div>' . $sub['label'] . '</div>
                                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                          stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                          class="feather feather-chevron-right">
                                          <polyline points="9 18 15 12 9 6"></polyline>
                                      </svg>
                                  </a>
                                  <ul id="submenu-' . $row['id'] . '-' . $sub['id'] . '" class="collapse" data-parent="#compact_submenuSidebar">';
              foreach ($sub['children'] as $last) {
                $submenu .= '
                                        <li><a href="' . Url::to($last['url']) . '"> ' . $last['label'] . ' </a></li>
                                        ';
              }
              $submenu .= '
                                  </ul>
                              </li>
                              ';
            }
          }

          $submenu .= '
                      </ul>
                  </div>
                  ';
        }
      }

      echo $menu;
      ?>
    </ul>

    <div class="">

      <div class="dropdown user-profile-dropdown">
        <a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="userProfileDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img src="<?= Url::base(true); ?>/themes/cork/images/user.png" class="img-fluid" alt="avatar">
        </a>
        <div class="dropdown-menu position-absolute" aria-labelledby="userProfileDropdown">
          <div class="dropdown-inner">
            <div class="user-profile-section">
              <div title="Edit User" class="media mx-auto showModalButton pointer" href="#" value="<?= Url::to(['/sdm/user/update', 'id_user' => Yii::$app->user->id]); ?>">
                <img src="<?= Url::base(true); ?>/themes/cork/images/user.png" class="img-fluid mr-2" alt="avatar">
                <i class="fal fa-user img-fluid mr-2" alt="avatar"></i>
                <div class="media-body">
                  <h5><?= !empty(Yii::$app->user->identity->username) ? Yii::$app->user->identity->username : "" ?></h5>
                  <p>User</p>
                </div>
              </div>
            </div>
            <div class="dropdown-item">
              <a href="<?= Url::to(['/default/logout']) ?>">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-out">
                  <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path>
                  <polyline points="16 17 21 12 16 7"></polyline>
                  <line x1="21" y1="12" x2="9" y2="12"></line>
                </svg>
                <span>Log Out</span>
              </a>
            </div>

          </div>
        </div>
      </div>

    </div>

  </nav>

  <div id="compact_submenuSidebar" class="submenu-sidebar ps">

    <?php
    echo $submenu;
    ?>

  </div>
</div>
