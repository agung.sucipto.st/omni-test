<?php
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = $name;
?>

<div class="row">
	<div class="col-sm-6 special">
		<img src="<?=Url::base()?>/themes/cork/images/404-page.gif" class="img-fluid"/>
	</div>
	<div class="col-sm-6 pt-5">
		<h1 style="font-weight: bold" class="text-warning"><?=$this->title?></h1>

		<i class="lead">
			<?= nl2br(Html::encode($message)) ?>
			<br />
			<?php

					echo '<br><br><h2>Silahkan Hubungi Administrator</h2>';
			?>
      agung.sucipto.st@gmail.com<br/>
      0858-3711-9884
		</i>
	</div>
</div>
