<div class="container">
  <div class="row">
    <div class="col-6">
      <img src="<?= \yii\helpers\Url::base(true); ?>/themes/cork/images/cassa_text.png" class="img-fluid" alt="logo">
    </div>
    <div class="col-6">
      <img src="<?= \yii\helpers\Url::base(true); ?>/themes/cork/images/cassa_text_dark.png" class="img-fluid" alt="logo">
    </div>
  </div>
</div>
