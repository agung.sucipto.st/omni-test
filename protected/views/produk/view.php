<?php
use \yii\web\View;
use app\widgets\JSRegister;
$this->title = 'Produk Detail';
?>

<style>
  .list {
    aspect-ratio: auto 4 / 3;
    height: 100px;
    overflow:hidden;
    border:1px solid #ccc;
    margin-top:10px;
    margin-bottom:10px;
  }

  .rating {
      unicode-bidi: bidi-override;
      text-align: center;
  }

  .star {
      display: inline-block;
      margin: 0 5px;
      font-size: 24px; /* Adjust the size as needed */
  }

  .star::before {
      content: "\2605"; /* Unicode character for a filled star (★) */
  }

  .star.empty::before {
      content: "\2606"; /* Unicode character for an empty star (☆) */
  }

  .star.half::before {
      content: "\2605"; /* Unicode character for a filled star (★) */
      position: absolute;
      overflow: hidden;
      width: 50%; /* Display half of the star */
  }

</style>

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-6">
      <img src="" alt="" class="img-fluid" id="mainThumb" width="100%">
      <div class="row" id="listImage">

      </div>
    </div>
    <div class="col-sm-6">
      <div class="row">
        <div class="col-sm-6">
          <h4>
            Price:
            $<span id="price"></span>
          </h4>
        </div>
        <div class="col-sm-6">
          <div class="rating" id="starRating"></div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6">
          <b>Category : <span id="categoryVal"></span></b>
        </div>
        <div class="col-sm-6">
          <b>Brand : <span id="brandVal"></span></b>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6">
          <b>Stock : <span id="stockVal"></span></b>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <b>Description : </b>
          <p id="desc"></p>
        </div>
      </div>
    </div>
  </div>
</div>

<?php JSRegister::begin(['position' => View::POS_END]);?>
<script>
  $.ajax({
    type: "GET",
    url: 'https://dummyjson.com/products/<?=$id?>',
    headers: { 'Access-Control-Allow-Origin': '*' }, cors: true, crossDomain: true,
    success: function(response){
      console.log(response)
      $("#listImage").html('');
      $("#mainThumb").attr('src',response.thumbnail);
      $("#price").html(response.price);
      $("#categoryVal").html(response.category);
      $("#brandVal").html(response.brand);
      $("#stockVal").html(response.stock);
      $("#desc").html(response.description);
      createStarRating(parseFloat(response.rating));
      if(response.images.length){
        response.images.forEach(item => {
          $("#listImage").append(`<div class="col-sm-3"><img src="${item}" alt="" class="img-fluid list" onClick="replace('${item}')"></div>`);
        });
      }
    }
});

function replace(url){
  $("#mainThumb").attr('src',url);
}

function createStarRating(rating) {
    const ratingContainer = document.getElementById('starRating');
    console.log(rating);
    for (let i = 1; i <= 5; i++) {
        const star = document.createElement('span');
        star.classList.add('star');

        if (i <= rating) {
            star.classList.add('filled');
        } else {
            star.classList.add('empty');
        }

        ratingContainer.appendChild(star);
    }
    $("#starRating").append(rating);
}
</script>
<?php JSRegister::end(); ?>
