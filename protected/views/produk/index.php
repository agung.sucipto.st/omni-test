<?php
use \yii\web\View;
use yii\helpers\Url;
use app\widgets\JSRegister;

$this->title = 'Produk';
?>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <button class="btn btn-primary" onClick="showData()">Show Data</button>
      <table class="table" id="produk">
        <thead>
          <tr>
            <th>No</th>
            <th>Image</th>
            <th>Title</th>
            <th>Category</th>
            <th>Rating</th>
            <th>Brand</th>
            <th>Stok</th>
            <th>Price</th>
            <th>Action</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>

<?php yii\bootstrap4\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    'options' => [
        'data-backdrop' => 'static',
        'tabindex'      => false,
    ],
]);
echo "<div id='modalContent'></div>";
yii\bootstrap4\Modal::end();

$viewUrl               = Url::to(['view']);
?>

<?php JSRegister::begin(['position' => View::POS_END]);?>
<script>
  var table;

  table = $('#produk').DataTable( {
      ajax: {
          url: 'https://dummyjson.com/products',
          crossDomain: true,
          dataSrc: 'products',
      },
      columns: [
        { data: 'id' },
        {
            data: 'thumbnail',
            render: function (data) {
                return '<img src="' + data + '" alt="Product Image" width="100">';
            },
        },
        { data: 'title' },
        { data: 'category' },
        { data: 'rating' },
        { data: 'brand' },
        { data: 'stock' },
        {
            data: 'price',
            render: function (data) {
                return '$' + data.toFixed(2);
            },
        },
        {
            data: 'id',
            render: function (data, type, row) {
                return '<button class="btn btn-primary btn-sm showModalButton" title="'+row.title+'" value="<?=$viewUrl; ?>?id='+data+'">View</button>';
            },
        },
    ],
  } );

  function showData(){
    table.ajax.url('https://dummyjson.com/products').load();
    $('#produk').show();
  }
</script>
<?php JSRegister::end(); ?>
