<?php

use yii\helpers\Html;
use app\modules\rbac\components\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */

$this->title = 'Tambah Menu';
$this->params['breadcrumbs'][] = ['label' => 'Menu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['pageIcon'] = 'fas fa-edit';
$this->params['pageHeader'] = $this->title;
$this->params['pageActionButton'][] = Helper::checkRoute('index') ? Html::a('<i class="fa fa-arrow-left"></i> Kembali', ['index'], ['class' => 'btn btn-primary']) : null;
?>
<div class="menu-create col-lg-12">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
