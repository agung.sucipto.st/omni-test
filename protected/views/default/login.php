<?php
use \yii\helpers\Url;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>

<style>
  #card-login {
    margin: 40% 10px 0px 10px;
  }

  body {
    background-color: gainsboro;
  }

  #overlay {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: black;
    opacity: 0.3;
  }

  @media screen and (min-width: 701px) {
    #card-login {
      margin: 20% 0px 0px 0px;
    }

    body {
      background-image: url('<?=Url::base(true); ?>/themes/cork/images/background.jpg');
      background-position: center;
      background-size: cover;
    }
  }
</style>

<div class="card" id="card-login">
  <div class="card-body">
    <div id="login-form">
      <div style="display: flex;justify-content: center;align-items: center;">
        <img src="<?=Url::base(true); ?>/themes/cork/img-client/logo.png" alt="cassalogo" style="width: 150px;">
      </div>
      <div style="display: flex;justify-content: center;align-items: center;">
        <h4><?=Yii::$app->name?></h4>
      </div>
      <br>
      <?php $form = ActiveForm::begin(['class'=>'text-left']); ?>

        <?= $form->field($model, 'username') ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <div class="row">
          <div class="col-12">
            <?= $form->field($model, 'rememberMe')->checkbox() ?>
          </div>
        </div>
         <div class="text-right">
           <?= Html::submitButton('<i class="fa fa-user"></i> Login', ['class' => 'btn btn-lg btn-dark', 'name' => 'login-button']) ?>
         </div>
        <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>
