
<?php

  use yii\helpers\Url;
  use yii\helpers\Html;
  use app\widgets\JSRegister;
  use yii\web\View;
  use app\models\User;

  $this->title = 'Home';
  $User = User::findOne(Yii::$app->user->id);

?>

<style>
  #placeWeight {
    width:100%;
    height: 100px;
    background: #90e61d;
    color: #000;
    font-size: 50pt;
    font-family: Arial !important;
    font-weight: bold;
    text-align:right;
    margin-bottom:10px;
    border-radius:10px;
    padding-right:30px;
    padding-bottom:10px;
    border: 5px solid #000;
  }
  .table > tbody tr {
    border-bottom: none !important;
  }
</style>

<blockquote class="blockquote mb-0">

    <div class="row">
        <div class="col-2 text-center">
            <img src="<?= \yii\helpers\Url::base(true); ?>/themes/cork/images/logo.png" class="img-fluid" alt="logo">
        </div>
        <div class="col-4">
              <h3> Home </h3>
              Selamat Datang <b> <?= $User->username; ?> </b>
              Silahkan gunakan aplikasi sesuai hak akses yang telah diberikan. Jika terjadi permasalahan, silahkan hubungi Admin.
        </div>
    </div>

</blockquote>
