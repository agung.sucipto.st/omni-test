<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\widgets\Pjax;
use yii\web\View;
use app\widgets\JSRegister;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PesananSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pesanan';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php yii\bootstrap4\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    'options' => [
        'data-backdrop' => 'static',
        'tabindex'      => false,
    ],
]);
echo "<div id='modalContent'></div>";
yii\bootstrap4\Modal::end();
?>

<div class="col-lg-12 text-right">
    <?=  Html::button('<i class="fal fa-plus-circle"></i> Tambah', ['value' => Url::to(['create']), 'title' => 'Form '.'Pesanan', 'class' => 'showModalButton btn btn-primary']); ?>
</div>
<hr/>
<div class="pesanan-index col-lg-12 p-0 table-responsive">

<?php Pjax::begin(['id' => 'gridData', 'timeout' => false, 'enablePushState' => true, 'enableReplaceState' => false]); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'no_pesanan',
            'tanggal',
            'nm_supplier',
            'nm_produk',
            'total',
            [
                'header'=>'#',
                'format'=>'raw',
                'value' => function ($model) {
                    return Html::button('<i class="fal fa-file-search"></i>', ['value' => Url::to(['view', 'no_pesanan' => $model->no_pesanan]), 'title' => 'Detail '.'Pesanan', 'class' => 'showModalButton btn btn-dark mb-2 mr-2 btn-rounded']).
                    Html::button('<i class="fal fa-edit"></i>', ['value' => Url::to(['update', 'no_pesanan' => $model->no_pesanan]), 'title' => 'Form Edit '.'Pesanan', 'class' => 'showModalButton btn btn-dark mb-2 mr-2 btn-rounded']).
                    Html::button('<i class="fal fa-trash"></i>', ['no_pesanan' => $model->no_pesanan, 'title' => 'Hapus Data?', 'class' => 'btn btn-dark mb-2 mr-2 btn-rounded delete']);
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

<?php
    $link_delete               = Url::to(['delete']);
    JSRegister::begin(['position' => View::POS_END]);
?>
    <script>
        var is_fetch = false;
        $(document).on("click", ".delete", function () {
            var id               = $(this).attr("no_pesanan");
            var link_delete            = "<?= $link_delete; ?>?no_pesanan="+id;
            Swal.fire({
            title: 'Konfirmasi',
            text: 'Apakah Akan Menghapus Data?',
            showCancelButton: true,
            confirmButtonText: 'Lanjutkan',
            cancelButtonText: 'Batal',
            icon: 'question',
            }).then((result) => {
                if (result.isConfirmed) {
                    if(is_fetch == false) {
                        is_fetch = true;
                        $.ajax({
                            type: "POST",
                            url: link_delete,
                            dataType: "html",
                            success: function(response){
                              const backRes = JSON.parse(response);
                              if(backRes.status == 'success') {
                                  Swal.fire('Ok', backRes.message, 'success');
                                  $.pjax.reload({container:"#gridData"});
                              } else {
                                  Swal.fire('Terjadi Kesalahan.', backRes.message, 'warning');
                              }
                              is_fetch = false
                            }
                        });
                    }
                }
            })
        });
    </script>
<?php JSRegister::end(); ?>
