<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pesanan */

$this->title = $model->no_pesanan;
$this->params['breadcrumbs'][] = ['label' => 'Pesanan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pesanan-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'no_pesanan',
            'tanggal',
            'nm_supplier',
            'nm_produk',
            'total',
        ],
    ]) ?>

</div>
