<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\web\View;
use app\widgets\JSRegister;

/* @var $this yii\web\View */
/* @var $model app\models\Pesanan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pesanan-form">

    <?php $form = ActiveForm::begin([
        'enableClientScript' => true,
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => 'form',
        'formConfig' => [
            'labelSpan' => 3,
            'deviceSize' => ActiveForm::SIZE_X_SMALL
        ]
    ]); ?>

    <?= $form->field($model, 'no_pesanan')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'tanggal')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Enter event time ...'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]);
    ?>

    <?= $form->field($model, 'nm_supplier')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nm_produk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total')->textInput() ?>

    <hr/>
    <div class="form-group text-right">
        <?= Html::submitButton('<i class="fal fa-save"></i> '.'Simpan', ['class' => 'btn btn-primary', "id"=>"btn-save"]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php    JSRegister::begin(['position' => View::POS_END]);
?>
<script>
    $('#form').submit(function(e){
      e.preventDefault()
    });
    $(document).on("click", "#btn-save", function () {
        var link_simpan     = $('#form').attr('action');
        var data_form       = $('#form').serializeArray();

        Swal.fire({
            title: 'Konfirmasi',
            text: 'Apakah Akan Menyimpan Data?',
            showCancelButton: true,
            confirmButtonText: 'Lanjutkan',
            cancelButtonText: 'Batal',
            icon: 'question',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "POST",
                        url: link_simpan,
                        data: data_form,
                        dataType: "html",
                        success: function(response){
                          const res = JSON.parse(response);
                          if(res.status == 'success') {
                              $('#modal').modal('hide');
                              Swal.fire('OK', res.message, 'success');
                              $.pjax.reload({container:"#gridData"});
                          } else {
                              Swal.fire('Terjadi Kesalahan.', res.message, 'warning');
                          }
                        }
                    });
                }
            })
    });
</script>
<?php JSRegister::end(); ?>
