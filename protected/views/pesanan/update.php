<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pesanan */

$this->title = 'Update Pesanan: ' . $model->no_pesanan;
?>
<div class="pesanan-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
