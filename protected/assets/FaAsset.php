<?php

namespace app\assets;

use yii\web\AssetBundle;


class FaAsset extends AssetBundle
{
    public $basePath = '@app/themes/cork';
    public $baseUrl = '@web/themes/cork';
    public $css = [
        'plugins/font-icons/fontawesome/css/regular.css',
        'plugins/font-icons/fontawesome/css/fontawesome.css',
    ];
    public $js = [
    ];
    public $depends = [
    ];
}
