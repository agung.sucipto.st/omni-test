<?php

namespace app\assets;

use yii\web\AssetBundle;


class LoginAsset extends AssetBundle
{
    public $basePath = '@app/themes/cork';
    public $baseUrl = '@web/themes/cork';
    public $css = [
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
