<?php

namespace app\assets;

use yii\web\AssetBundle;


class MDFontAsset extends AssetBundle
{
  public $basePath = '@app/themes/MaterialDesignWebfont';
  public $baseUrl = '@web/themes/MaterialDesignWebfont';
  public $css = [
    'css/materialdesignicons.css'
  ];
  public $js = [];
  public $depends = [];
}
