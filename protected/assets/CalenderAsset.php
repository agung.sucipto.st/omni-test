<?php

namespace app\assets;

use yii\web\AssetBundle;


class CalenderAsset extends AssetBundle
{
  public $basePath = '@app/themes/calender';
  public $baseUrl = '@web/themes/calender';
  public $css = [
    'main.css',
  ];
  public $js = [
    'main.js',
  ];
  public $depends = [];
}
