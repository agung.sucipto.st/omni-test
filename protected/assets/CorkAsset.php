<?php

namespace app\assets;

use yii\web\AssetBundle;


class CorkAsset extends AssetBundle
{
    public $basePath = '@app/themes/cork';
    public $baseUrl = '@web/themes/cork';
    public $css = [
        'assets/css/plugins.css',
        'assets/css/authentication/form-2.css',
        'assets/css/users/user-profile.css',
        'assets/css/plugins.css',
        'plugins/table/datatable/datatables.css',
        'plugins/table/datatable/dt-global_style.css',
        'assets/css/replacecss.css',
        'assets/css/all.min.css',
        'assets/css/custom.css',
        'assets/css/elements/custom-pagination.css',
        'assets/css/components/tabs-accordian/custom-tabs.css',
        'assets/css/widgets/modules-widgets.css',
    ];
    public $js = [
        'bootstrap/js/popper.min.js',
        'plugins/perfect-scrollbar/perfect-scrollbar.min.js',
        'assets/js/app.js',
        'assets/js/init.js',
        'assets/js/custom.js',
        'assets/js/modal.js',
        'assets/js/freeze-table.js',
        'assets/js/jquery.PrintArea.js',
        'plugins/table/datatable/datatables.js',
        'assets/js/swal-fire.js',
        'assets/js/jquery.table2excel.js',
        'plugins/apex/apexcharts.min.js',
        'assets/js/socket.io.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset'
    ];
}
