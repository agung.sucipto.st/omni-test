<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\modules\rbac\components\RouteRule;
use app\modules\rbac\components\Configs;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel mdm\admin\models\searchs\AuthItem */
/* @var $context mdm\admin\components\ItemController */

$context = $this->context;
$labels = $context->labels();
$this->title = Yii::t('rbac-admin', $labels['Items']);
$this->params['breadcrumbs'][] = $this->title;

$rules = array_keys(Configs::authManager()->getRules());
$rules = array_combine($rules, $rules);
unset($rules[RouteRule::RULE_NAME]);
?>
<div class="role-index">
    <div class="card">
        <div class="card-body">
            <p>
                <?= Html::a(Yii::t('rbac-admin', 'Create ' . $labels['Item']), ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'kartik\grid\SerialColumn','mergeHeader' => false],
                    [
                        'attribute' => 'name',
                        'label' => Yii::t('rbac-admin', 'Name'),
                    ],
                    [
                        'attribute' => 'ruleName',
                        'label' => Yii::t('rbac-admin', 'Rule Name'),
                        'filter' => $rules
                    ],
                    [
                        'attribute' => 'description',
                        'label' => Yii::t('rbac-admin', 'Description'),
                    ],
                    [
                        'width' => '10%',
                        'class' => 'kartik\grid\ActionColumn',
                        'mergeHeader' => false,
                        'template' => ('{view}{update}{delete}'),
                        'buttons' => [
                            'view' => function ($url, $model) use ($labels) {
                                $buton = Html::a('<i class="fa fa-search text-info"></i>',
                                    Yii::$app->urlManager->createUrl(['rbac/'.$labels['item'].'/view', 'id' => $model->name]),
                                    [
                                        'class' => 'btn m-btn m-btn--icon btn-sm m-btn--icon-only'
                                    ]
                                );
                                return $buton;
                            },
                            'update' => function ($url, $model) use ($labels) {
                                $buton = Html::a('<i class="fa fa-edit text-info"></i>',
                                    Yii::$app->urlManager->createUrl(['rbac/'.$labels['item'].'/update', 'id' => $model->name]),
                                    [
                                        'class' => 'btn m-btn m-btn--icon btn-sm m-btn--icon-only'
                                    ]
                                );
                                return $buton;
                            },
                            'delete' => function ($url, $model) use ($labels) {
                                $buton = Html::a('<i class="fa fa-trash text-danger"></i>',
                                    Yii::$app->urlManager->createUrl(['rbac/'.$labels['item'].'/delete', 'id' => $model->name]),
                                    [
                                        'data-method' => 'post',
                                        'data-confirm' => '<strong>Yakin akan dihapus?</strong>',
                                        'class' => 'btn m-btn m-btn--icon btn-sm m-btn--icon-only'
                                    ]
                                );
                                return $buton;
                            },
                        ],
                    ],
                ],
            ])
            ?>

        </div>
    </div>
</div>
