<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel mdm\admin\models\searchs\Menu */

$this->title = Yii::t('rbac-admin', 'Menus');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">

    <div class="card">
        <div class="card-body">
            <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

            <p>
                <?= Html::a(Yii::t('rbac-admin', 'Create Menu'), ['create'], ['class' => 'btn btn-primary']) ?>
            </p>

            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'name',
                    [
                        'attribute' => 'menuParent.name',
                        'filter' => Html::activeTextInput($searchModel, 'parent_name', [
                            'class' => 'form-control', 'id' => null
                        ]),
                        'label' => Yii::t('rbac-admin', 'Parent'),
                    ],
                    'route',
                    'order',
                    [
                        'width' => '10%',
                        'class' => 'kartik\grid\ActionColumn',
                        'mergeHeader' => false,
                        'template' => ('{view}{update}{delete}'),
                        'buttons' => [
                            'view' => function ($url, $model) {
                                $buton = Html::a('<i class="fa fa-search text-info"></i>',
                                    Yii::$app->urlManager->createUrl(['rbac/menu/view', 'id' => $model->id]),
                                    [
                                        'class' => 'btn m-btn m-btn--icon btn-sm m-btn--icon-only'
                                    ]
                                );
                                return $buton;
                            },
                            'update' => function ($url, $model) {
                                $buton = Html::a('<i class="fa fa-edit text-info"></i>',
                                    Yii::$app->urlManager->createUrl(['rbac/menu/update', 'id' => $model->id]),
                                    [
                                        'class' => 'btn m-btn m-btn--icon btn-sm m-btn--icon-only'
                                    ]
                                );
                                return $buton;
                            },
                            'delete' => function ($url, $model) {
                                $buton = Html::a('<i class="fa fa-trash text-danger"></i>',
                                    Yii::$app->urlManager->createUrl(['rbac/menu/delete', 'id' => $model->id]),
                                    [
                                        'data-method' => 'post',
                                        'data-confirm' => '<strong>Yakin akan dihapus?</strong>',
                                        'class' => 'btn m-btn m-btn--icon btn-sm m-btn--icon-only'
                                    ]
                                );
                                return $buton;
                            },
                        ],
                    ],
                ],
            ]);
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
