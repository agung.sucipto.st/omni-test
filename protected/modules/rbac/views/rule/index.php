<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this  yii\web\View */
/* @var $model mdm\admin\models\BizRule */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel mdm\admin\models\searchs\BizRule */

$this->title = Yii::t('rbac-admin', 'Rules');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-index">
    <div class="card">
        <div class="card-header">
            <h4><?= 'List '.Yii::t('rbac-admin', 'Rules') ?></h4>
        </div>
        <div class="card-body">
            <p>
                <?= Html::a(Yii::t('rbac-admin', 'Create Rule'), ['create'], ['class' => 'btn btn-primary']) ?>
            </p>

            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'name',
                        'label' => Yii::t('rbac-admin', 'Name'),
                    ],
                    [
                        'width' => '10%',
                        'class' => 'kartik\grid\ActionColumn',
                        'mergeHeader' => false,
                        'template' => ('{view}{update}{delete}'),
                        'buttons' => [
                            'view' => function ($url, $model) {
                                $buton = Html::a('<i class="fa fa-search text-info"></i>',
                                    Yii::$app->urlManager->createUrl(['rbac/rule/view', 'id' => $model->name]),
                                    [
                                        'class' => 'btn m-btn m-btn--icon btn-sm m-btn--icon-only'
                                    ]
                                );
                                return $buton;
                            },
                            'update' => function ($url, $model) {
                                $buton = Html::a('<i class="fa fa-edit text-info"></i>',
                                    Yii::$app->urlManager->createUrl(['rbac/rule/update', 'id' => $model->name]),
                                    [
                                        'class' => 'btn m-btn m-btn--icon btn-sm m-btn--icon-only'
                                    ]
                                );
                                return $buton;
                            },
                            'delete' => function ($url, $model) {
                                $buton = Html::a('<i class="fa fa-trash text-danger"></i>',
                                    Yii::$app->urlManager->createUrl(['rbac/rule/delete', 'id' => $model->name]),
                                    [
                                        'data-method' => 'post',
                                        'data-confirm' => '<strong>Yakin akan dihapus?</strong>',
                                        'class' => 'btn m-btn m-btn--icon btn-sm m-btn--icon-only'
                                    ]
                                );
                                return $buton;
                            },
                        ],
                    ],
                ],
            ]);
            ?>
        </div>
    </div>
</div>
