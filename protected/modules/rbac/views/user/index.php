<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\modules\rbac\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel mdm\admin\models\searchs\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('rbac-admin', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <div class="card">
        <div class="card-header">
            <h4><i class="fa fa-users"></i> List Pengguna</h4>
        </div>
        <div class="card-body">
            <?php
            $column = [
                ['class' => 'yii\grid\SerialColumn'],
                'username',
                'email:email',
                [
                    'attribute' => 'status',
                    'value' => function ($model) {
                        return $model->status == 0 ? 'Inactive' : 'Active';
                    },
                    'filter' => [
                        0 => 'Inactive',
                        10 => 'Active'
                    ]
                ],
                [
                    'width' => '10%',
                    'class' => 'kartik\grid\ActionColumn',
                    'mergeHeader' => false,
                    'template' => ('{view}{delete}'),
                    'buttons' => [
                        'view' => function ($url, $model) {
                            $buton = Html::a('<i class="fa fa-search text-info"></i>',
                                Yii::$app->urlManager->createUrl(['rbac/user/view', 'id' => $model->id]),
                                [
                                    'class' => 'btn m-btn m-btn--icon btn-sm m-btn--icon-only'
                                ]
                            );
                            return $buton;
                        },

                        'delete' => function ($url, $model) {
                            $buton = Html::a('<i class="fa fa-trash text-danger"></i>',
                                Yii::$app->urlManager->createUrl(['rbac/user/delete', 'id' => $model->id]),
                                [
                                    'data-method' => 'post',
                                    'data-confirm' => '<strong>Yakin akan dihapus?</strong>',
                                    'class' => 'btn m-btn m-btn--icon btn-sm m-btn--icon-only'
                                ]
                            );
                            return $buton;
                        },
                    ],
                ],
            ];

            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'condensed' => false,
                'striped' => false,
                'columns' => $column
            ]);
            ?>
        </div>
    </div>

</div>
