<?php

namespace app\components;

use app\models\KasJenisTransaksi;
use yii\base\UserException;
use yii\db\Query;

class NoKeuangan
{
    private static function getCodeKas($key)
    {
        $d = KasJenisTransaksi::find()->where(['id_jenis_transaksi' => $key])->one();
        return $d->kode;
    }

    public static function kas($id_jenis_transaksi, $tanggal = null)
    {
        $month = !empty($tanggal) ? date("n", strtotime($tanggal)) : date("n");
        $tahun = !empty($tanggal) ? date("Y", strtotime($tanggal)) : date("Y");

        $q = new Query();
        $q->select('id_kas_transaksi')->from('kas_transaksi')
            ->where(['EXTRACT(MONTH FROM time_create)' => $month])
            ->andWhere(['EXTRACT(YEAR FROM time_create)' => $tahun])
            ->andWhere(['id_jenis_transaksi' => $id_jenis_transaksi]);

        $count = sprintf("%05s", $q->count() + 1);
        return self::getCodeKas($id_jenis_transaksi) . $count .'/'. $month .'/'. $tahun;
    }

    public static function piutang($tanggal = null)
    {
        $month = !empty($tanggal) ? date("n", strtotime($tanggal)) : date("n");
        $tahun = !empty($tanggal) ? date("Y", strtotime($tanggal)) : date("Y");

        $q = new Query();
        $q->select('id_piutang')->from('piutang')
            ->where(['EXTRACT(MONTH FROM time_create)' => $month])
            ->andWhere(['EXTRACT(YEAR FROM time_create)' => $tahun]);

        $count = sprintf("%05s", $q->count() + 1);
        return $count .'/INV/'. $month .'/'. $tahun;
    }

    public static function hutang($tanggal = null)
    {
        $month = !empty($tanggal) ? date("n", strtotime($tanggal)) : date("n");
        $tahun = !empty($tanggal) ? date("Y", strtotime($tanggal)) : date("Y");

        $q = new Query();
        $q->select('id_hutang')->from('hutang')
            ->where(['EXTRACT(MONTH FROM time_create)' => $month])
            ->andWhere(['EXTRACT(YEAR FROM time_create)' => $tahun]);

        $count = sprintf("%05s", $q->count() + 1);
        return $count .'/APP/'. $month .'/'. $tahun;
    }
}
