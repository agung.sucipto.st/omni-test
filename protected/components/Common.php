<?php

namespace app\components;

use Yii;

class Common
{
    const STATUS_SUCCESS = "success";
    const STATUS_ERROR = "error";
    const YESNO = [
        1 => 'Ya',
        0 => 'Tidak'
    ];

    /**
     * @return false|string
     */
    public static function dateTimeNow()
    {
        return date('Y-m-d H:i:s');
    }

    /**
     * @return int|string
     */
    public static function userLoggedIn()
    {
        return isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->getId() : 1;
    }

    /**
     * @param $status
     * @param $message
     * @return array
     */
    public static function resultAction($status, $message)
    {
        return ["status" => $status, "message" => $message];
    }

    /**
     * @param $model
     * @return false|mixed|string
     */
    public static function getModelName($model)
    {
        $array = explode("\\", get_class($model));
        return end($array);
    }

    /**
     * @param $request
     * @param $model
     * @param $bodyParams
     * @return void
     */
    public static function setParams($request, $model, $bodyParams)
    {
        Yii::$app->request->setQueryParams([
            'r' => $request->getPathInfo(),
            self::getModelName($model) => $bodyParams
        ]);
    }
}
