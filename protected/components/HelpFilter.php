<?php

    namespace app\components;

    use Yii;
    use yii\helpers\Url;
    use app\models\Unit;
    use app\models\Jabatan;
    use app\models\Kas;
    use app\models\JenisHargaJual;
    use yii\helpers\Html;

    class HelpFilter
    {
        public static function FilterJenisCatatanPegawai()
        {
            return [
                ''                  => '',
                'Reward'            => 'Reward',
                'Punishment'        => 'Punishment',
            ];
        }
        public static function FilterTahun()
        {
            $tahun_mulai    = 2000;
            $tahun_sampai   = 2050;
            $data_tampil[]  = null;
            for ($tahun_mulai = 2000; $tahun_mulai <= $tahun_sampai; $tahun_mulai++) {
                $data_tampil[$tahun_mulai] = $tahun_mulai;
            }
            return $data_tampil;

        }
        public static function FilterBulan()
        {
            return [
                '01'    => 'Januari',
                '02'    => 'Februari',
                '03'    => 'Maret',
                '04'    => 'April',
                '05'    => 'Mei',
                '06'    => 'Juni',
                '07'    => 'Juli',
                '08'    => 'Agustus',
                '09'    => 'September',
                '10'    => 'Oktober',
                '11'    => 'November',
                '12'    => 'Desember'
            ];
        }
        public static function FilterHari()
        {
            return [
                'Monday'        => 'Senin',
                'Tuesday'       => 'Selasa',
                'Wednesday'     => 'Rabu',
                'Thursday'      => 'Kamis',
                'Friday'        => 'Jumat',
                'Saturday'      => 'Sabtu',
                'Sunday'        => 'Minggu',
            ];
        }
        public static function FilterKelompokKomponenGaji()
        {
            return [
                ''                      => '',
                'Pokok'                 => 'Pokok',
                'Tunjangan Tetap'       => 'Tunjangan Tetap',
                'Tunjangan Tidak Tetap' => 'Tunjangan Tidak Tetap',
                'Iuran'                 => 'Iuran',
                'Potongan Lain'         => 'Potongan Lain',
                'Pajak'                 => 'Pajak',
            ];
        }
        public static function FilterKomponen()
        {
            return [
                ''                  => '',
                'Karyawan'          => 'Karyawan',
                'Perusahaan'        => 'Perusahaan',
            ];
        }
        public static function FilterJenisKomponenGaji()
        {
            return [
                ''          => '',
                '+'         => '+',
                '-'         => '-',
            ];
        }
        public static function FilterJenisKontrak()
        {
            return [
                ''                  => '',
                'Tetap'             => 'Tetap',
                'Kontrak 1'         => 'Kontrak 1',
                'Kontrak 2'         => 'Kontrak 2',
            ];
        }
        public static function FilterJenisJadwal()
        {
            return [
                ''              => '',
                'Pagi'          => 'Pagi',
                'Siang'         => 'Siang',
                'Malam'         => 'Malam',
            ];
        }
        public static function FilterJenisKelamin()
        {
            return [
                ''                  => '',
                'Laki-laki'         => 'Laki-laki',
                'Perempuan'         => 'Perempuan',
            ];
        }
        public static function FilterAgama()
        {
            return [
                'Islam'             => 'Islam',
                'Kristen Protestan' => 'Kristen Protestan',
                'Kristen Katolik'   => 'Kristen Katolik',
                'Hindu'             => 'Hindu',
                'Buddha'            => 'Buddha',
                'Khonghucu'         => 'Khonghucu',
            ];
        }
        public static function FilterStatusPernikahan()
        {
            return [
                'Belum Kawin'       => 'Belum Kawin',
                'Kawin'             => 'Kawin',
                'Cerai Hidup'       => 'Cerai Hidup',
                'Cerai Mati'        => 'Cerai Mati',
            ];
        }
        public static function FilterPendidikan()
        {
            return [
                'Tidak/ Belum Sekolah'                  => 'Tidak/ Belum Sekolah',
                'Belum Tamat SD/ Sederajat'             => 'Belum Tamat SD/ Sederajat',
                'Tamat SD/ Sederajat'                   => 'Tamat SD/ Sederajat',
                'SLTP/ Sederajat'                       => 'SLTP/ Sederajat',
                'SLTA/ Sederajat'                       => 'SLTA/ Sederajat',
                'Diploma I/ II'                         => 'Diploma I/ II',
                'Akademi/ Diploma III /Sarjana Muda'    => 'Akademi/ Diploma III /Sarjana Muda',
                'Diploma IV/ Sastra I'                  => 'Diploma IV/ Sastra I',
                'Sastra II'                             => 'Sastra II',
                'Sastra III'                            => 'Sastra III',
            ];
        }
        public static function FilterJenisJadwalLembur()
        {
            return [
                ''                 => '',
                'Hari Kerja'       => 'Hari Kerja',
                'Hari Libur'       => 'Hari Libur',
            ];
        }
        public static function FilterJenisPerhitunganLembur()
        {
            return [
                ''              => '',
                'Jam'           => 'Jam',
                'Kehadiran'     => 'Kehadiran',
            ];
        }

        public static function FilterJenisHargaJual()
        {
            $data_array         = JenisHargaJual::find()->where(['is_aktif'=> 1])->orderby('nama_grup')->all();
            $data_tampil[]      = null;
            foreach($data_array as $list) {
                $data_tampil[$list->id_jenis_harga_jual] = $list->nama_grup;
            }
            return $data_tampil;
        }

        public static function FilterKas()
        {
            $data_array         = Kas::find()->where(['is_aktif'=> 1])->orderby('nama_kas')->all();
            $data_tampil[]      = null;
            foreach($data_array as $list) {
                $data_tampil[$list->id_kas] = $list->nama_kas;
            }
            return $data_tampil;
        }
    }

?>