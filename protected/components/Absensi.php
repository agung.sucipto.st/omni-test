<?php

    namespace app\components;

    use Yii;
    use yii\helpers\Url;
    use yii\helpers\Html;
    use DateTime;
    use app\models\FingerData;
    use app\models\PegawaiJadwal;
    use app\models\Jadwal;

    class Absensi
    {
        public static function PeriksaMasukAtauPulang($list_arr_PegawaiJadwal)
        {
            
            $scan_masuk                                             = Absensi::ScanMasuk($list_arr_PegawaiJadwal);
            $scan_pulang                                            = Absensi::ScanPulang($list_arr_PegawaiJadwal);

            // echo '<pre>'; print_r($scan_pulang); exit();

            // return [
            //     'scan_masuk' => $scan_masuk,
            //     'scan_pulang' => $scan_pulang,
            // ];

            if($scan_masuk != null) {
                $list_arr_PegawaiJadwal->is_hadir                   = $scan_masuk['is_hadir'];
                $list_arr_PegawaiJadwal->waktu_masuk                = $scan_masuk['scan_masuk'];
                $list_arr_PegawaiJadwal->is_terlambat               = $scan_masuk['is_terlambat'];
                $list_arr_PegawaiJadwal->total_jam_terlambat        = $scan_masuk['total_jam_terlambat'];
            }

            if($scan_pulang != null) {
                $list_arr_PegawaiJadwal->waktu_pulang               = $scan_pulang['scan_pulang'];
                $list_arr_PegawaiJadwal->is_pulang_cepat            = $scan_pulang['is_pulang_cepat'];
                $list_arr_PegawaiJadwal->total_jam_pulang_cepat     = $scan_pulang['total_jam_pulang_cepat'];
            }

            if($scan_masuk != null && $scan_pulang != null) {
                $list_arr_PegawaiJadwal->total_jam_kerja_real       = Absensi::TotalJamKerjaReal($list_arr_PegawaiJadwal->waktu_masuk, $list_arr_PegawaiJadwal->waktu_pulang);
            }

            if($list_arr_PegawaiJadwal->save(false)) {
                return true;
            } else {
                return false;
            }
        }

        public static function ScanMasuk($list_arr_PegawaiJadwal)
        {
            $tanggal                    = $list_arr_PegawaiJadwal->tanggal;
            $id_absensi                 = $list_arr_PegawaiJadwal->pegawai->id_absensi;
            $jam_masuk                  = $list_arr_PegawaiJadwal->jadwal->waktu_masuk;
            $date_jam_masuk             = $tanggal.' '.$jam_masuk;              
            $date_paling_cepat          = date('Y-m-d H:i:s', strtotime("-180 minutes", strtotime($tanggal.' '.$jam_masuk)));
            $date_paling_lambat         = date('Y-m-d H:i:s', strtotime("+180 minutes", strtotime($tanggal.' '.$jam_masuk)));

             // perlu dicari
            $data_FingerData_by_id_absensi  = null;
            $scan_masuk                     = null;
            $is_hadir                       = $list_arr_PegawaiJadwal->is_hadir;
            $is_terlambat                   = $list_arr_PegawaiJadwal->is_terlambat;
            $total_jam_terlambat            = null;

            $cari_FingerData_by_id_absensi  = FingerData::find()
                ->Where(['between', 'waktu_finger', $date_paling_cepat, $date_jam_masuk])
                ->andWhere(['id_absensi'=> $id_absensi])
                ->orderby(['waktu_finger'=> SORT_ASC])
                ->one();

                if(isset($cari_FingerData_by_id_absensi)) {
                    $data_FingerData_by_id_absensi = $cari_FingerData_by_id_absensi;
                } else {

                    $cari_FingerData_by_id_absensi  = FingerData::find()
                    ->Where(['between', 'waktu_finger', $date_jam_masuk, $date_paling_lambat])
                    ->andWhere(['id_absensi'=> $id_absensi])
                    ->orderby(['waktu_finger'=> SORT_DESC])
                    ->one();

                    if(isset($cari_FingerData_by_id_absensi)) {
                        $data_FingerData_by_id_absensi = $cari_FingerData_by_id_absensi;
                    }
                }

                if($data_FingerData_by_id_absensi != null) {

                    // $waktu_scan             = explode(" ",$data_FingerData_by_id_absensi->waktu_finger);
                    // $scan_masuk             = $waktu_scan[1];

                    // $awal  = date_create($date_jam_masuk);
                    // $akhir = date_create('2022-03-14 05:30:00');
                    // $diff  = date_diff( $akhir, $awal );

                    // echo '<pre>'; print_r($diff); 

                    // echo '<pre>'; print_r($date_jam_masuk); 
                    // echo '<pre>'; print_r('2022-03-14 07:30:00'); 
                    // echo '<pre>'; print_r($diff->h.':'.$diff->i.':'.$diff->s); 
                    // exit();


                    $waktu_scan             = explode(" ",$data_FingerData_by_id_absensi->waktu_finger);
                    $scan_masuk             = $waktu_scan[1];
                    $date_masuk             = strtotime($date_jam_masuk);
                    $date_scan              = strtotime($data_FingerData_by_id_absensi->waktu_finger);
                    $diff                   = $date_scan - $date_masuk;
                    $jam                    = floor($diff / (60 * 60));
                    $menit                  = $diff - ( $jam * (60 * 60) );
                    $detik                  = $diff % 60;
                    $is_hadir               = 1;
                    $is_terlambat           = $diff > 0 ? 1 : 0;
                    $total_jam_terlambat    = $diff > 0 ? $jam .':'. floor( $menit / 60 ) .':'. $detik : '00:00:00';

                    return [
                        'jam_masuk'             => $jam_masuk,
                        'scan_masuk'            => $data_FingerData_by_id_absensi->waktu_finger,
                        'is_hadir'              => $is_hadir,
                        'is_terlambat'          => $is_terlambat,
                        'total_jam_terlambat'   => $total_jam_terlambat,
                    ];

                }

            return null;

        }

        public static function ScanPulang($list_arr_PegawaiJadwal)
        {

            $tanggal                    = $list_arr_PegawaiJadwal->tanggal;
            $id_absensi                 = $list_arr_PegawaiJadwal->pegawai->id_absensi;
            $jam_pulang                 = $list_arr_PegawaiJadwal->jadwal->waktu_keluar;
            $date_jam_pulang            = $tanggal.' '.$jam_pulang;              
            $date_paling_cepat          = date('Y-m-d H:i:s', strtotime("-180 minutes", strtotime($tanggal.' '.$jam_pulang)));
            $date_paling_lambat         = date('Y-m-d H:i:s', strtotime("+180 minutes", strtotime($tanggal.' '.$jam_pulang)));

            // perlu dicari
            $data_FingerData_by_id_absensi  = null;
            $scan_pulang                    = null;
            $is_pulang_cepat                = $list_arr_PegawaiJadwal->is_pulang_cepat;
            $total_jam_pulang_cepat         = null;


            $cari_FingerData_by_id_absensi  = FingerData::find()
                ->Where(['between', 'waktu_finger', $date_jam_pulang, $date_paling_lambat])
                ->andWhere(['id_absensi'=> $id_absensi])
                ->orderby(['waktu_finger'=> SORT_ASC])
                ->one();

                if(isset($cari_FingerData_by_id_absensi)) {
                    $data_FingerData_by_id_absensi = $cari_FingerData_by_id_absensi;
                } else {

                    $cari_FingerData_by_id_absensi  = FingerData::find()
                    ->Where(['between', 'waktu_finger', $date_paling_cepat, $date_jam_pulang])
                    ->andWhere(['id_absensi'=> $id_absensi])
                    ->orderby(['waktu_finger'=> SORT_DESC])
                    ->one();

                    if(isset($cari_FingerData_by_id_absensi)) {
                        $data_FingerData_by_id_absensi = $cari_FingerData_by_id_absensi;
                    }
                }

                if($data_FingerData_by_id_absensi != null) {

                    $waktu_scan             = explode(" ",$data_FingerData_by_id_absensi->waktu_finger);
                    $scan_pulang            = $waktu_scan[1];
                    $date_pulang             = strtotime($date_jam_pulang);
                    $date_scan              = strtotime($data_FingerData_by_id_absensi->waktu_finger);

                    $diff                   = $date_pulang - $date_scan;
                    $jam                    = floor($diff / (60 * 60));
                    $menit                  = $diff - ( $jam * (60 * 60) );
                    $detik                  = $diff % 60;
                    $is_pulang_cepat        = floor($diff) > 0 ? 1 : 0;
                    $total_jam_pulang_cepat = floor($diff) > 0 ? $jam .':'. floor( $menit / 60 ) .':'. $detik : '00:00:00';

                    return [
                        'jam_pulang'                => $jam_pulang,
                        'scan_pulang'               => $data_FingerData_by_id_absensi->waktu_finger,
                        'is_pulang_cepat'           => $is_pulang_cepat,
                        'total_jam_pulang_cepat'    => $total_jam_pulang_cepat,
                    ];

                }

            return null;
        }

        public static function TotalJamKerja($id_jadwal)
        {
            $Jadwal     = Jadwal::findOne($id_jadwal);
            $awal       = new DateTime(date("Y-m-d").' '.$Jadwal->waktu_masuk);
            $akhir      = new DateTime(date("Y-m-d").' '.$Jadwal->waktu_keluar);
            $diff       = $awal->diff($akhir);

            return $diff->h.':'.$diff->i.':'.$diff->s;
        }

        public static function TotalJamKerjaReal($waktu_masuk, $waktu_pulang)
        {
            if($waktu_masuk != null && $waktu_pulang != null) {
                $awal  = new DateTime($waktu_masuk);
                $akhir = new DateTime($waktu_pulang);
                $diff  = $awal->diff($akhir);
    
                return $diff->h.':'.$diff->i.':'.$diff->s;
            }
            return '00:00:00';
        }

        public static function TotalJamLembur($waktu_mulai, $waktu_selesai)
        {
            if($waktu_mulai != null && $waktu_selesai != null) {
                $awal  = new DateTime($waktu_mulai);
                $akhir = new DateTime($waktu_selesai);
                $diff  = $awal->diff($akhir);
    
                return $diff->h.':'.$diff->i.':'.$diff->s;
            }
            return '00:00:00';
        }
    }

?>