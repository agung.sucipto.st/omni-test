<?php

namespace app\components;

use app\models\JenisTransaksi;
use yii\base\UserException;
use yii\db\Query;

class NomorTransaksi
{
    const PEMESANAN_INTERNAL = "pemesanan_internal";
    const PEMESANAN_EKSTERNAL = "pemesanan_eksternal";

    private static function getKode($key)
    {
        $d = JenisTransaksi::find()->where(['key' => $key])->one();
        return $d["kode"];
    }

    /**
     * @param $key
     * @return mixed
     * @throws UserException
     */
    public static function getId($key)
    {
        $d = JenisTransaksi::find()->where(['key' => $key])->one();

        if (empty($d)) throw new UserException("Key " . $key . " tidak ditemukan");

        return $d["id_jenis_transaksi"];
    }

    public static function nomorPemesanan($jenisTransaksi, $tanggal = null)
    {
        $key = $jenisTransaksi == "internal" ? self::PEMESANAN_INTERNAL : self::PEMESANAN_EKSTERNAL;
        $month = !empty($tanggal) ? date("n", strtotime($tanggal)) : date("n");
        $tahun = !empty($tanggal) ? date("Y", strtotime($tanggal)) : date("Y");

        $q = new Query();
        $q->select('id_pemesanan')->from('pemesanan')
            ->where(['EXTRACT(MONTH FROM tanggal_pemesanan)' => $month])
            ->andWhere(['EXTRACT(YEAR FROM tanggal_pemesanan)' => $tahun])
            ->andWhere(['jenis_pemesanan' => $jenisTransaksi]);

        $count = sprintf("%05s", $q->count() + 1);
        return self::getKode($key) . $count . $month . $tahun;
    }

    public static function nomorItemTransaksi($keyJenisTransaksi, $tanggalTransaksi = null)
    {
        $idJenisTransaksi = self::getId($keyJenisTransaksi);
        $month = !empty($tanggalTransaksi) ? date("n", strtotime($tanggalTransaksi)) : date("n");
        $tahun = !empty($tanggalTransaksi) ? date("Y", strtotime($tanggalTransaksi)) : date("Y");

        $q = new Query();
        $q->select('id_item_transaksi')->from('item_transaksi')
            ->where(['EXTRACT(MONTH FROM tanggal_transaksi)' => $month])
            ->andWhere(['EXTRACT(YEAR FROM tanggal_transaksi)' => $tahun])
            ->andWhere(['id_jenis_transaksi' => $idJenisTransaksi]);

        $count = sprintf("%05s", $q->count() + 1);
        return ['nomor' => self::getKode($keyJenisTransaksi) . $count . $month . $tahun, 'idJenisTransaksi' => $idJenisTransaksi];
    }

    public static function nomorSo($tanggalTransaksi)
    {

        $month = !empty($tanggalTransaksi) ? date("n", strtotime($tanggalTransaksi)) : date("n");
        $tahun = !empty($tanggalTransaksi) ? date("Y", strtotime($tanggalTransaksi)) : date("Y");

        $q = new Query();
        $q->select('id')->from('stok_opname')
            ->where(['EXTRACT(MONTH FROM tanggal)' => $month])
            ->andWhere(['EXTRACT(YEAR FROM tanggal)' => $tahun]);

        $count = sprintf("%03s", $q->count() + 1);
        return "SO/OPN/" . $count . "/" . $month . "/" . $tahun;
    }
}
