<?php

namespace app\components;

use Yii;

class Lib
{
    public static function tglIndo($ex, $type, $day = true)
    {
        if ($ex == NULL) return '-';
        if ($ex == '0000-00-00') return '-';
        if ($ex == '0000-00-00') return '-';
        if ($ex == '0000-00-00 00:00:00') return '-';

        if ($type == '00-bulan-0000') {
            if ($day == true) {
                $pecah = explode(" ", $ex);
                $tglsaja = $pecah[0];
            } else {
                $tglsaja = $ex;
            }
            $pecahtanggal = explode("-", $tglsaja);
            $bulan = Lib::getBulan(ltrim($pecahtanggal[1], '0'));

            return $pecahtanggal[2] . ' ' . $bulan . ' ' . $pecahtanggal[0];
        } else
            if ($type == 'bulan') {
                if ($day == true) {
                    $pecah = explode(" ", $ex);
                    $tglsaja = $pecah[0];
                } else {
                    $tglsaja = $ex;
                }
                $pecahtanggal = explode("-", $tglsaja);
                $bulan = Lib::getBulan(ltrim($pecahtanggal[1], '0'));

                return $bulan;
            } else if ($type == '00-bul-0000') {
                $pecah = explode(" ", $ex);

                $tglsaja = $pecah[0];
                $pecahtanggal = explode("-", $tglsaja);
                $bulan = Lib::getBulansingkat(ltrim($pecahtanggal[1], '0'));

                return $pecahtanggal[2] . '-' . $bulan . '-' . $pecahtanggal[0];
            } else if ($type == '00 bul 0000') {
                $pecah = explode(" ", $ex);

                $tglsaja = $pecah[0];
                $pecahtanggal = explode("-", $tglsaja);
                $bulan = Lib::getBulansingkat(ltrim($pecahtanggal[1], '0'));

                return $pecahtanggal[2] . ' ' . $bulan . ' ' . $pecahtanggal[0];
            } else if ($type == '00-00-0000') {
                $pecah = explode(" ", $ex);
                $tglsaja = $pecah[0];
                $pecahtanggal = explode("-", $tglsaja);
                $bulan = $pecahtanggal[1];

                return $pecahtanggal[2] . '-' . $bulan . '-' . $pecahtanggal[0];
            } else if ($type == 'Hari 00-BulanSingkat-0000 WIB') {
                $pecah = explode(" ", $ex);
                $nameofDay = '';
                if ($day == true) {
                    $nameofDay = Lib::getNamaHari($pecah[0]) . ', ';
                } else {
                    $nameofDay = '';
                }
                if (!empty($pecah[1])) {
                    $tgl = $pecah[0];
                    $tanggal = substr($tgl, 8, 2);
                    $bulan = Lib::getBulan(substr($tgl, 5, 2));
                    $tahun = substr($tgl, 0, 4);
                    return $nameofDay . $tanggal . ' ' . substr($bulan, 0, 3) . ' ' . $tahun . ' ' . $pecah[1] . ' WIB';
                }
            } else if ($type == '00-BulanSingkat-0000 WIB') {
                $pecah = explode(" ", $ex);
                $nameofDay = '';
                if ($day == true) {
                    $nameofDay = Lib::getNamaHari($pecah[0]) . ', ';
                } else {
                    $nameofDay = '';
                }
                if (!empty($pecah[1])) {
                    $tgl = $pecah[0];
                    $tanggal = substr($tgl, 8, 2);
                    $bulan = Lib::getBulan(substr($tgl, 5, 2));
                    $tahun = substr($tgl, 0, 4);
                    return $tanggal . ' ' . substr($bulan, 0, 3) . ' ' . $tahun . ' ' . $pecah[1] . ' WIB';
                }

            } else if ($type == '00-BulanSingkat-0000 TANPA WAKTU') {
                $pecah = explode(" ", $ex);
                $nameofDay = '';
                if ($day == true) {
                    $nameofDay = Lib::getNamaHari($pecah[0]) . ', ';
                } else {
                    $nameofDay = '';
                }
                $tgl = $pecah[0];
                $tanggal = substr($tgl, 8, 2);
                $bulan = Lib::getBulan(substr($tgl, 5, 2));
                $tahun = substr($tgl, 0, 4);
                return $tanggal . ' ' . substr($bulan, 0, 3) . ' ' . $tahun;
            } else if ($type == '00-BulanSingkat-0000 WIB(wDetik)') {
                $pecah = explode(" ", $ex);
                $nameofDay = '';
                if ($day == true) {
                    $nameofDay = Lib::getNamaHari($pecah[0]) . ', ';
                } else {
                    $nameofDay = '';
                }
                if (!empty($pecah[1])) {
                    $tgl = $pecah[0];
                    $tanggal = substr($tgl, 8, 2);
                    $bulan = Lib::getBulan(substr($tgl, 5, 2));
                    $tahun = substr($tgl, 0, 4);
                    return $tanggal . ' ' . substr($bulan, 0, 3) . ' ' . $tahun . ' ' . substr($pecah[1], 0, 5) . ' WIB';
                }
            } else if ($type == '00-00-0000 (wDetik)') {
                $pecah = explode(" ", $ex);
                $nameofDay = '';
                if ($day == true) {
                    $nameofDay = Lib::getNamaHari($pecah[0]) . ', ';
                } else {
                    $nameofDay = '';
                }
                if (!empty($pecah[1])) {
                    $tgl = $pecah[0];
                    $tanggal = substr($tgl, 8, 2);
                    $bulan = substr($tgl, 5, 2);
                    $tahun = substr($tgl, 0, 4);
                    return $tanggal . '-' . $bulan . '-' . $tahun . ' ' . substr($pecah[1], 0, 5);
                }
            }
    }

    public static function getBulan($bln)
    {
        switch ($bln) {
            case 1:
                return "Januari";
                break;
            case 2:
                return "Februari";
                break;
            case 3:
                return "Maret";
                break;
            case 4:
                return "April";
                break;
            case 5:
                return "Mei";
                break;
            case 6:
                return "Juni";
                break;
            case 7:
                return "Juli";
                break;
            case 8:
                return "Agustus";
                break;
            case 9:
                return "September";
                break;
            case 10:
                return "Oktober";
                break;
            case 11:
                return "November";
                break;
            case 12:
                return "Desember";
                break;
        }
    }

    public static function getBulansingkat($bln)
    {
        switch ($bln) {
            case 1:
                return "Jan";
                break;
            case 2:
                return "Feb";
                break;
            case 3:
                return "Mar";
                break;
            case 4:
                return "Apr";
                break;
            case 5:
                return "May";
                break;
            case 6:
                return "Jun";
                break;
            case 7:
                return "Jul";
                break;
            case 8:
                return "Aug";
                break;
            case 9:
                return "Sep";
                break;
            case 10:
                return "Oct";
                break;
            case 11:
                return "Nov";
                break;
            case 12:
                return "Dec";
                break;
        }
    }

    public static function getNamaHari($date)
    {
        $namahari = date('D', strtotime($date));
        //Function date(String1, strtotime(String2)); adalah fungsi untuk mendapatkan nama hari
        return Lib::getHari($namahari);
    }

    public static function getHari($hari)
    {
        switch ($hari) {
            case 'Mon':
                return "Senin";
                break;
            case 'Tue':
                return "Selasa";
                break;
            case 'Wed':
                return "Rabu";
                break;
            case 'Thu':
                return "Kamis";
                break;
            case 'Fri':
                return "Jumat";
                break;
            case 'Sat':
                return "Sabtu";
                break;
            case 'Sun':
                return "Minggu";
                break;
        }
    }

    public static function rupiah($nilai, $pecahan = 0)
    {
        return number_format($nilai, $pecahan, ',', '.');
    }

    function terbilang($x, $style = 4)
    {
        if ($x == 0) {
            $hasil = "nol rupiah";
        } else if ($x < 0) {
            $hasil = "minus " . trim(Lib::kekata($x));
        } else {
            $hasil = trim(Lib::kekata($x)) . ' rupiah';
        }
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }
        return $hasil;
    }

    function kekata($x)
    {
        $x = abs($x);
        $angka = array("", "satu", "dua", "tiga", "empat", "lima",
            "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($x < 12) {
            $temp = " " . $angka[$x];
        } else if ($x < 20) {
            $temp = Lib::kekata($x - 10) . " belas";
        } else if ($x < 100) {
            $temp = Lib::kekata($x / 10) . " puluh" . Lib::kekata($x % 10);
        } else if ($x < 200) {
            $temp = " seratus" . Lib::kekata($x - 100);
        } else if ($x < 1000) {
            $temp = Lib::kekata($x / 100) . " ratus" . Lib::kekata($x % 100);
        } else if ($x < 2000) {
            $temp = " seribu" . Lib::kekata($x - 1000);
        } else if ($x < 1000000) {
            $temp = Lib::kekata($x / 1000) . " ribu" . Lib::kekata($x % 1000);
        } else if ($x < 1000000000) {
            $temp = Lib::kekata($x / 1000000) . " juta" . Lib::kekata($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = Lib::kekata($x / 1000000000) . " milyar" . Lib::kekata(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = Lib::kekata($x / 1000000000000) . " trilyun" . Lib::kekata(fmod($x, 1000000000000));
        }
        return $temp;
    }

    public static function UploadScale($file)
    {
        //identitas file asli
        ob_start();
        $source_url = $file;
        $info = getimagesize($source_url);
        if ($info['mime'] == 'image/jpeg') {
            $im_src = imagecreatefromjpeg($source_url);
            //$ext='.jpg';
        } elseif ($info['mime'] == 'image/gif') {
            $im_src = imagecreatefromgif($source_url);
            //$ext='.gif';
        } elseif ($info['mime'] == 'image/png') {
            $im_src = imagecreatefrompng($source_url);
            //$ext='.png';
        }
        $src_width = imageSX($im_src);
        $src_height = imageSY($im_src);

        //Simpan dalam versi small 350 pixel
        //Set ukuran gambar hasil perubahan

        $imageratio = $src_width / $src_height;
        $dst_width = 280;
        $dst_height = 280 / $imageratio;

        //proses perubahan ukuran
        $im = imagecreatetruecolor($dst_width, $dst_height);
        imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

        //Simpan gambar
        if ($info['mime'] == 'image/png') {
            $background = imagecolorallocate($im, 0, 0, 0);
            imagecolortransparent($im, $background);
            imagealphablending($im, false);
            imagesavealpha($im, true);
            imagepng($im, null);
        } else {
            imagejpeg($im, null);
        }

        $image_data = ob_get_contents();
        ob_end_clean();

        //Hapus gambar di memori komputer
        imagedestroy($im_src);
        imagedestroy($im);

        return [
            'ext' => $info['mime'],
            'stream' => $image_data
        ];
    }

    public static function UploadCompress($file)
    {
        //Simpan gambar dalam ukuran sebenarnya
        ob_start();
        $source_url = $file;
        $info = getimagesize($source_url);

        if ($info['mime'] == 'image/jpeg') {
            $image = imagecreatefromjpeg($source_url);
            //$ext='.jpg';
        } elseif ($info['mime'] == 'image/gif') {
            $image = imagecreatefromgif($source_url);
            //$ext='.gif';
        } elseif ($info['mime'] == 'image/png') {
            $image = imagecreatefrompng($source_url);
            //$ext='.png';
        }

        imagejpeg($image, null, 50);
        $image_data = ob_get_contents();
        ob_end_clean();

        imagedestroy($image);
        return [
            'ext' => $info['mime'],
            'stream' => $image_data
        ];
    }
}
