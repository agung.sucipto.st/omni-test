<?php

namespace app\components;

use Yii;
use app\models\WaLog;

class Wa
{
    public static function send($data)
    {
      /*
      $data
      [
          'phone' => 'number with formating 62',
          'msg' => 'messsage to send',
          'image' => 'image as base64 format'
      ]
      */
      /*
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'http://103.31.39.35:5000/send-wa');
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      $payload = json_encode($data);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $content = curl_exec($ch);
      $res =json_decode($content);
      if($res && !$res->status) {
        $log = new WaLog();
        $log->phone = $data['phone'];
        $log->message = $data['msg'];
        $log->image = $data['image'];
        $log->save();
      }
      $err = curl_error($ch);
      */
    }

    public static function getImageDataFromUrl($url)
    {
        $urlParts = pathinfo($url);
        $extension = $urlParts['extension'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $base64 = 'data:image/' . $extension . ';base64,' . base64_encode($response);
        return $base64;
    }

    public static function base64Image($data)
    {
        $base64 = 'data:'.$data['ext'].';base64,' . base64_encode($data['stream']);
        return $base64;
    }
}
