<?php

use yii\db\Migration;

class m230820_000820_create_foreign_keys extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey(
            'menu_parent_fkey',
            '{{%menu}}',
            ['parent'],
            '{{%menu}}',
            ['id'],
            'SET NULL',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('menu_parent_fkey', '{{%menu}}');
    }
}
