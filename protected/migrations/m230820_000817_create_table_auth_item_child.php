<?php

use yii\db\Migration;

class m230820_000817_create_table_auth_item_child extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%auth_item_child}}',
            [
                'parent' => $this->string(64)->notNull(),
                'child' => $this->string(64)->notNull(),
            ],
            $tableOptions
        );

        $this->addPrimaryKey('auth_item_child_pkey', '{{%auth_item_child}}', ['parent', 'child']);

        $this->addForeignKey(
            'auth_item_child_parent_fkey',
            '{{%auth_item_child}}',
            ['parent'],
            '{{%auth_item}}',
            ['name'],
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'auth_item_child_child_fkey',
            '{{%auth_item_child}}',
            ['child'],
            '{{%auth_item}}',
            ['name'],
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%auth_item_child}}');
    }
}
