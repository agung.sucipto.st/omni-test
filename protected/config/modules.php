<?php
return [
    'rbac' => [
        'class' => 'app\modules\rbac\rbac',
        'layout' => 'left-menu',
        'mainLayout' => '@app/views/layouts/main_rbac.php',
        'controllerMap' => [
            'assignment' => [
                'class' => 'app\modules\rbac\controllers\AssignmentController',
                'userClassName' => 'app\models\User',
                'idField' => 'user_id'
            ]
        ],
        'menus' => [
            'assignment' => [
                'label' => 'Grant Access'
            ],
        ],
    ],
    'gridview' => [
        'class' => '\kartik\grid\Module',
        // your other grid module settings
    ],
    'gridviewKrajee' => [
        'class' => '\kartik\grid\Module',
        // your other grid module settings
    ]
];
