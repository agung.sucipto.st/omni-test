$(function () {
  $(document).on("click", ".showModalButton", function () {
    const dismiss =
      '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>';
      const loader = '<div class="d-flex justify-content-center"><div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div></div>';
    if ($("#modal").data("bs.modal").isShown) {
      $("#modal").find("#modalContent").html(loader).load($(this).attr("value"));
      //dynamiclly set the header for the modal
      document.getElementById("modalHeader").innerHTML =
        "<h6>" + $(this).attr("title") + "</h6>" + dismiss;
    } else {
      //if modal isn't open; open it and load content
      $("#modal")
        .modal("show")
        .find("#modalContent")
        .html(loader)
        .load($(this).attr("value"));
      //dynamiclly set the header for the modal
      document.getElementById("modalHeader").innerHTML =
        "<h6><i class=\"fal fa-paste\"></i> " + $(this).attr("title") + "</h6>" + dismiss;
    }
  });
});

function onModal(title,url) {
  const dismiss =
    '<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>';
  const loader =
    '<div class="d-flex justify-content-center bd-highlight mb-3"><div class="lds-dual-ring"><div></div><div></div></div></div>';
  if ($("#modal").data("bs.modal").isShown) {
    $("#modal")
      .find("#modalContent")
      .html(loader)
      .load(url, function (res, status, xhr) {
        if (status == "error") {
          $("#modal").modal("hide");
          swal("Proses Gagal", res, "error");
        }
        $(this).html(res);
      });
    $("#modalHeader").html(
      '<h5 class="modal-title"><i class="fal fa-paste"></i> ' + title + "</h5>" + dismiss
    );
  } else {
    $("#modal")
      .modal("show")
      .find("#modalContent")
      .html(loader)
      .load(url, function (res, status, xhr) {
        if (status == "error") {
          $("#modal").modal("hide");
          swal("Proses Gagal", res, "error");
        }
        $(this).html(res);
      });
    $("#modalHeader").html(
      '<h5 class="modal-title"><i class="fal fa-paste"></i> ' + title + "</h5>" + dismiss
    );
  }
}
